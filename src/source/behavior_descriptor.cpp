/*!*******************************************************************************************
 *  \file       behavior_descriptor.cpp
 *  \brief      BehaviorDescriptor implementation file.
 *  \details    This file implements the BehaviorDescriptor class.
 *  \authors    Alberto Camporredondo.
 *  \copyright  Copyright (c) 2018 Universidad Politecnica de Madrid
 *              All rights reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#include "../include/behavior_descriptor.h"

/*Constructors*/
BehaviorDescriptor::BehaviorDescriptor()
{
}

BehaviorDescriptor::BehaviorDescriptor(std::string name, std::string category, bool default_activation, int timeout,
                                       std::map<int, std::vector<std::string>> processes,
                                       std::vector<ArgumentDescriptor> arguments)
{
  this->name = name;
  this->category = category;
  this->default_activation = default_activation;
  this->timeout = timeout;
  this->processes = processes;
  this->arguments = arguments;
}

/*Destructor*/
BehaviorDescriptor::~BehaviorDescriptor()
{
}

/*Functionality*/
aerostack_msgs::BehaviorCommand BehaviorDescriptor::serialize()
{
  aerostack_msgs::BehaviorCommand behavior_msg;
  behavior_msg.name = name;

  return behavior_msg;
}

/*Getters*/
std::string BehaviorDescriptor::getName()
{
  return name;
}

std::string BehaviorDescriptor::getCategory()
{
  return category;
}

bool BehaviorDescriptor::isActivatedByDefault()
{
  return default_activation;
}

int BehaviorDescriptor::getTimeout()
{
  return timeout;
}

std::map<int, std::vector<std::string>> BehaviorDescriptor::getProcesses()
{
  return processes;
}

std::vector<ArgumentDescriptor> BehaviorDescriptor::getArguments()
{
  return arguments;
}

/*Setters*/
void BehaviorDescriptor::setName(std::string name)
{
  this->name = name;
}

void BehaviorDescriptor::setCategory(std::string category)
{
  this->category = category;
}

void BehaviorDescriptor::setDefaultActivation(bool default_activation)
{
  this->default_activation = default_activation;
}

void BehaviorDescriptor::setTimeout(int timeout)
{
  this->timeout = timeout;
}

void BehaviorDescriptor::setProcesses(std::map<int, std::vector<std::string>> processes)
{
  this->processes = processes;
}

void BehaviorDescriptor::setArguments(std::vector<ArgumentDescriptor> arguments)
{
  this->arguments = arguments;
}
